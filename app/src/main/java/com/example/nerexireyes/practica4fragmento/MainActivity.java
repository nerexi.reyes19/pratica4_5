package com.example.nerexireyes.practica4fragmento;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Frguno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener {
    Button botonFranUn, botonFranDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonFranUn = (Button) findViewById(R.id.btnFrgUn);
        botonFranDos = (Button) findViewById(R.id.btnFrgDos);
        botonFranUn.setOnClickListener(this);
        botonFranDos.setOnClickListener(this);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:

                Dialog dialogLogin = new Dialog(MainActivity.this);
                dialogLogin.setContentView(R.layout.dlg_log);

                Button botonAutenticar = (Button) dialogLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave=(EditText) dialogLogin.findViewById(R.id.txtPassw);

                botonAutenticar.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG );
                    }
                });
                dialogLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUn:
                Frguno fragmentoUno = new Frguno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break ;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
